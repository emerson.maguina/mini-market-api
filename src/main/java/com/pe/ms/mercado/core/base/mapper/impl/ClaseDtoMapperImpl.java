package com.pe.ms.mercado.core.base.mapper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.mapper.ClaseDtoMapper;
import com.pe.ms.mercado.core.base.model.entity.Clase;

/**
 * The Class ClaseDtoMapperImpl.
 */
@Component
public class ClaseDtoMapperImpl implements ClaseDtoMapper {

	/**
	 * As clase dto.
	 *
	 * @param src the src
	 * @return the clase dto
	 */
	@Override
	public ClaseDto asClaseDto(Clase src) {
		if (src == null) {
			return new ClaseDto();
		}

		ClaseDto claseDto = new ClaseDto();

		claseDto.setClassId(src.getClassId());
		claseDto.setDescription(src.getDescription());
		claseDto.setName(src.getName());

		return claseDto;
	}

	/**
	 * As clases dto.
	 *
	 * @param src the src
	 * @return the list
	 */
	@Override
	public List<ClaseDto> asClasesDto(List<Clase> src) {
		if (src == null) {
			return Collections.emptyList();
		}

		List<ClaseDto> list = new ArrayList<>(src.size());
		for (Clase clase : src) {
			list.add(asClaseDto(clase));
		}

		return list;
	}

	/**
	 * As clase.
	 *
	 * @param src the src
	 * @return the clase
	 */
	@Override
	public Clase asClase(ClaseDto src) {
		if (src == null) {
			return new Clase();
		}

		Clase clase = new Clase();

		clase.setDescription(src.getDescription());
		clase.setName(src.getName());

		return clase;
	}

}
