package com.pe.ms.mercado.core.base.exception;

public class ErrorFunctionalException extends Exception{
	private static final long serialVersionUID = 1L;

	public ErrorFunctionalException(String message) {
		super(message);
	}
}
