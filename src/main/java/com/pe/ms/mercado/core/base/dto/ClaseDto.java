/**
 * 
 */
package com.pe.ms.mercado.core.base.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class ClaseDto.
 *
 * @author emaguina
 */

/**
 * Gets the path image.
 *
 * @return the path image
 */
@Getter

/**
 * Sets the path image.
 *
 * @param pathImage the new path image
 */
@Setter

/**
 * Instantiates a new clase dto.
 */
@NoArgsConstructor

/**
 * Instantiates a new clase dto.
 *
 * @param classId the class id
 * @param name the name
 * @param description the description
 * @param pathImage the path image
 */
@AllArgsConstructor
public class ClaseDto {
	
	/** The class id. */
	private Long classId;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The path image. */
	private String pathImage;
}