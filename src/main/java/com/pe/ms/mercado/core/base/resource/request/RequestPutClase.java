package com.pe.ms.mercado.core.base.resource.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Gets the path image.
 *
 * @return the path image
 */

/**
 * Gets the updated user.
 *
 * @return the updated user
 */
@Getter

/**
 * Sets the path image.
 *
 * @param pathImage the new path image
 */

/**
 * Sets the updated user.
 *
 * @param updatedUser the new updated user
 */
@Setter

/**
 * Instantiates a new request post clase.
 */

/**
 * Instantiates a new request put clase.
 */
@NoArgsConstructor

/**
 * Instantiates a new request post clase.
 *
 * @param name the name
 * @param description the description
 * @param pathImage the path image
 */

/**
 * Instantiates a new request put clase.
 *
 * @param name the name
 * @param description the description
 * @param pathImage the path image
 * @param updatedUser the updated user
 */
@AllArgsConstructor
public class RequestPutClase {

	/** The name. */
	@Size(max = 30)
	@NotNull
	@NotBlank
	@ApiModelProperty(required = true, value = "")
	@JsonProperty("name")
	private String name;

	/** The description. */
	@Size(max = 100)
	@NotNull
	@NotBlank
	@ApiModelProperty(required = true, value = "")
	@JsonProperty("description")
	private String description;

	/** The path image. */
	@Size(max = 100)
	@ApiModelProperty(value = "")
	@JsonProperty("pathImage")
	private String pathImage;

	/** The updated user. */
	@Size(max = 11)
	@NotNull
	@NotBlank
	@ApiModelProperty(required = true, value = "")
	@JsonProperty("updatedUser")
	private String updatedUser;
}