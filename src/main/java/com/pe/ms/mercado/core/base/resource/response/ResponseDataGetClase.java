package com.pe.ms.mercado.core.base.resource.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pe.ms.mercado.core.base.resource.ClaseResource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new response data get clase.
 */
@NoArgsConstructor

/**
 * Instantiates a new response data get clase.
 *
 * @param classes the classes
 * @param total the total
 */
@AllArgsConstructor
public class ResponseDataGetClase {

	/** The classes. */
	@JsonProperty("class")
	private List<ClaseResource> classes;
	
	/** The total. */
	@JsonProperty("total")
	private int total;
}
