/**
 * 
 */
package com.pe.ms.mercado.core.base.mapper.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.mapper.ClaseResourceMapper;
import com.pe.ms.mercado.core.base.resource.ClaseResource;
import com.pe.ms.mercado.core.base.resource.request.RequestPostClase;
import com.pe.ms.mercado.core.base.resource.request.RequestPutClase;

// TODO: Auto-generated Javadoc
/**
 * The Class ClaseResourceMapperImpl.
 *
 * @author emaguina
 */
@Component
public class ClaseResourceMapperImpl implements ClaseResourceMapper {

	/**
	 * As clase resource.
	 *
	 * @param src the src
	 * @return the clase resource
	 */
	@Override
	public ClaseResource asClaseResource(ClaseDto src) {
		if (src == null) {
			return new ClaseResource();
		}

		ClaseResource claseResource = new ClaseResource();

		claseResource.setClassId(src.getClassId());
		claseResource.setDescription(src.getDescription());
		claseResource.setName(src.getName());

		return claseResource;
	}

	/**
	 * As clase resources.
	 *
	 * @param src the src
	 * @return the list
	 */
	@Override
	public List<ClaseResource> asClaseResources(List<ClaseDto> src) {
		if (src == null) {
			return Collections.emptyList();
		}

		List<ClaseResource> list = new ArrayList<>(src.size());
		for (ClaseDto claseDto : src) {
			list.add(asClaseResource(claseDto));
		}

		return list;
	}

	/**
	 * As clase dto.
	 *
	 * @param src the src
	 * @return the clase dto
	 */
	@Override
	public ClaseDto asClaseDto(RequestPostClase src) {
		if (src == null) {
			return new ClaseDto();
		}

		ClaseDto claseDto = new ClaseDto();

		claseDto.setDescription(src.getDescription());
		claseDto.setName(src.getName());

		return claseDto;
	}

	/**
	 * As clase dto.
	 *
	 * @param src the src
	 * @return the clase dto
	 */
	@Override
	public ClaseDto asClaseDto(RequestPutClase src) {
		if (src == null) {
			return new ClaseDto();
		}

		ClaseDto claseDto = new ClaseDto();

		claseDto.setDescription(src.getDescription());
		claseDto.setName(src.getName());

		return claseDto;
	}

}