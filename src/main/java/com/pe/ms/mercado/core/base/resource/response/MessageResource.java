package com.pe.ms.mercado.core.base.resource.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Setter;

@Setter
public class MessageResource {

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("success")
	private boolean success = true;
}
