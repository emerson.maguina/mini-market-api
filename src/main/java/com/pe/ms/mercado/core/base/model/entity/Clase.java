package com.pe.ms.mercado.core.base.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CLASS")
public class Clase implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Category Identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLASS_ID", nullable = false)
	private Long classId;

	/** Name Category. */
	@Column(name = "NAME", length = 30, nullable = false)
	private String name;

	/**
	 * Description Category
	 */
	@Column(name = "DESCRIPTION", length = 100, nullable = true)
	private String description;

	/**
	 * Path Image
	 */
	@Column(name = "PATH_IMAGE", length = 200, nullable = true)
	private String pathImage;

}