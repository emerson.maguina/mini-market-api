package com.pe.ms.mercado.core.base.resource.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pe.ms.mercado.core.base.resource.ClaseResource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new response data get clase.
 */

/**
 * Instantiates a new response data post clase.
 */
@NoArgsConstructor

/**
 * Instantiates a new response data get clase.
 *
 * @param classes the classes
 * @param total   the total
 */

/**
 * Instantiates a new response data post clase.
 *
 * @param clase the clase
 */
@AllArgsConstructor
public class ResponseDataPutClase {

	/** The classes. */
	@JsonProperty("class")
	private ClaseResource clase;

}
