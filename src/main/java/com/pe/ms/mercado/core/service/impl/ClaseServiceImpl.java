package com.pe.ms.mercado.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.exception.BadRequestException;
import com.pe.ms.mercado.core.base.exception.EmptyResultException;
import com.pe.ms.mercado.core.base.exception.ErrorFunctionalException;
import com.pe.ms.mercado.core.base.exception.ResourceNotFoundException;
import com.pe.ms.mercado.core.base.mapper.impl.ClaseDtoMapperImpl;
import com.pe.ms.mercado.core.base.model.entity.Clase;
import com.pe.ms.mercado.core.base.model.repository.ClaseRepository;
import com.pe.ms.mercado.core.service.ClaseService;
import com.pe.ms.mercado.util.Constante;
import com.pe.ms.mercado.util.Util;

/**
 * The Class ClaseServiceImpl.
 */
@Service
public class ClaseServiceImpl implements ClaseService {

	/** The messages. */
	@Autowired
	private MessageSource messages;

	/** The clase repository. */
	@Autowired
	private ClaseRepository claseRepository;

	/** The clase dto mapper. */
	@Autowired
	private ClaseDtoMapperImpl claseDtoMapper;

	/**
	 * Search classes.
	 *
	 * @param text   {@link String}
	 * @param page   {@link String}
	 * @param size   {@link String}
	 * @param sortBy {@link String}
	 * @return {@link List<ClassDto>}
	 * @throws BadRequestException  throw BadRequestException
	 * @throws EmptyResultException throw EmptyResultException
	 */
	public List<ClaseDto> getClasses(String text, int page, int size, String sortBy)
			throws BadRequestException, EmptyResultException {
		List<ClaseDto> classDtoList = new ArrayList<>();
		PageRequest pageRequest = null;

		try {
			pageRequest = PageRequest.of(page, size, Util.obtenerGroupBySort2String(sortBy));

			if (text == null || text.equals(Constante.EMPTY))
				classDtoList = claseDtoMapper.asClasesDto(claseRepository.getClasses(pageRequest));
			else
				classDtoList = claseDtoMapper
						.asClasesDto(claseRepository.getClassesAndNameOrDescription(text, pageRequest));
		} catch (Exception e) {
			throw new BadRequestException(messages.getMessage(Constante.MESSAGE_PARAMETERS_PAGING_INVALID, null,
					LocaleContextHolder.getLocale()));
		}

//		if (classDtoList.isEmpty()) {
//			throw new EmptyResultException();
//		}

		return classDtoList;
	}

	/**
	 * Count classes.
	 *
	 * @return {@link Integer}
	 */
	@Override
	public int getCountClasses() {
		return claseRepository.getCountClasses();
	}

	/**
	 * Creates the class.
	 *
	 * @param claseDto the clase dto
	 * @return the clase dto
	 * @throws ErrorFunctionalException the error functional exception
	 */
	@Override
	public ClaseDto createClass(ClaseDto claseDto) throws ErrorFunctionalException {
		Clase clase = claseRepository.findByName(claseDto.getName());

		if (clase != null)
			throw new ErrorFunctionalException(String.format(
					messages.getMessage(Constante.MESSAGE_NAME_EXISTS, null, LocaleContextHolder.getLocale()),
					claseDto.getName()));

		clase = claseDtoMapper.asClase(claseDto);
		return claseDtoMapper.asClaseDto(claseRepository.save(clase));
	}

	/**
	 * Update class.
	 *
	 * @param classId the class id
	 * @param claseDto the clase dto
	 * @return the clase dto
	 * @throws ResourceNotFoundException the resource not found exception
	 * @throws BadRequestException the bad request exception
	 * @throws ErrorFunctionalException the error functional exception
	 */
	@Override
	public ClaseDto updateClass(Long classId, ClaseDto claseDto)
			throws ResourceNotFoundException, BadRequestException, ErrorFunctionalException {
		Clase clase = claseRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException(String.format(
						messages.getMessage(Constante.MESSAGE_CLASS_NOT_FOUND, null, LocaleContextHolder.getLocale()),
						classId)));

		Clase classExistsName = claseRepository.findByName(claseDto.getName());

		if (classExistsName != null && !clase.getClassId().equals(classExistsName.getClassId()))
			throw new ErrorFunctionalException(String.format(
					messages.getMessage(Constante.MESSAGE_NAME_EXISTS, null, LocaleContextHolder.getLocale()),
					claseDto.getName()));

		Clase classNew = claseDtoMapper.asClase(claseDto);
		clase.setName(classNew.getName() != null ? classNew.getName() : clase.getName());
		clase.setDescription(classNew.getDescription() != null ? classNew.getDescription() : clase.getDescription());

		return claseDtoMapper.asClaseDto(claseRepository.save(clase));
	}

}
