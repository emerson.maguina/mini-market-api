package com.pe.ms.mercado.core.base.model.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pe.ms.mercado.core.base.model.entity.Clase;

@Repository
public interface ClaseRepository extends CrudRepository<Clase, Long>, JpaSpecificationExecutor<Clase> {

	/**
	 * Method that lists all Classes.
	 *
	 * @param pageable {@link Pageable} object that has paging data
	 * @return List<{@link Clase}> Classes list
	 */
	@Query("SELECT c FROM Clase c ")
	public List<Clase> getClasses(Pageable pageable);

	/**
	 * Method that allows listing the number of classes
	 * 
	 * @param pageable {@link Pageable} object that has paging data
	 * @return {@link int} Total Quantity of classes.
	 */
	@Query("SELECT count(c) FROM Clase c")
	public int getCountClasses();

	/**
	 * Query that returns class by name
	 * 
	 * @param name
	 * @return {@link Clase}
	 */
	public Clase findByName(String name);

	/**
	 * Method that lists all Classes
	 * 
	 * @param text     {@link String} name or description
	 * @param pageable {@link Pageable} object that has paging data
	 * @return List<{@link Clase}> Classes list for name or description
	 */
	@Query("SELECT c FROM Clase c WHERE LOWER(c.name) LIKE LOWER(CONCAT('%',:text,'%')) "
			+ " OR LOWER(c.description) LIKE LOWER(CONCAT('%',:text,'%')) ")
	public List<Clase> getClassesAndNameOrDescription(@Param("text") String text, Pageable pageable);

}
