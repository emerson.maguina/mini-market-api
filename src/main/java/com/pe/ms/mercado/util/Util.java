package com.pe.ms.mercado.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.jasypt.util.text.AES256TextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pe.ms.mercado.core.base.exception.BadRequestException;

public class Util {

	private Logger logger = LoggerFactory.getLogger(Util.class);

	/**
	 * Convert string to integer
	 *
	 * @param cadena {@link String}
	 * @return numero {@link int}
	 */
	public static int convertirStringInt(String cadena) {
		try {
			return Integer.parseInt(cadena);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Se han ingresado String en un campo que permite entero");
		}
	}

	/**
	 * Convert Date To String
	 *
	 * @param date {@link Date}
	 * @return date {@link String}
	 */
	public static String convertDateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * Convert Date To String (DateTime)
	 *
	 * @param date {@link Date}
	 * @return date {@link String}
	 */
	public static String convertDateTimeToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return sdf.format(date);
	}

	/**
	 * Convert string to integer
	 *
	 * @param cadena {@link String}
	 * @return numero {@link int}
	 */
	public static String getFielfromJsonProperty(String[] jsonProperty, String className, String pckg) {

		String responseFields = "";
		try {
			for (String value : jsonProperty) {
				String firstChar = value.substring(0, 1).equals("-") ? "-" : "";
				String formatted = firstChar.isEmpty() ? value : value.substring(1, value.length());
				String resource = String.format(Constante.RESOURCE_PACKAGE, pckg);
				Field[] fields = Class.forName(resource + "." + className).getDeclaredFields();
				for (Field f : fields) {
					f.setAccessible(true);
					JsonProperty annotation = f.getDeclaredAnnotation(JsonProperty.class);
					if (formatted.equals(annotation.value())) {
						responseFields = responseFields + firstChar + f.getName() + ",";
						break;
					}
				}
			}

			return responseFields;
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * Method that allows to get groupBySort
	 * 
	 * @param orden {@link String}
	 * @return {@link Sort}
	 */
	public static Sort obtenerGroupBySort2String(String sortBy) throws BadRequestException {

		StringTokenizer token = new StringTokenizer(sortBy, ",");
		Sort groupBySort = Sort.by(new Order[0]);
		String orden = "";
		while (token.hasMoreTokens()) {
			Sort fieldSort = Sort.by(new Order[0]);
			String field = token.nextToken().trim();
			if (field.substring(0, 1).equals("-")) {
				orden = obtenerOrdenDeBusqueda(field.substring(0, 1));
			} else {
				orden = "";
			}
			if (orden.isEmpty()) {
				fieldSort = Sort.by(field).ascending();
				groupBySort = groupBySort.and(fieldSort);
			} else {
				if (orden.equalsIgnoreCase(Constante.ORDEN_DESC)) {
					fieldSort = Sort.by(field.substring(1)).descending();
					groupBySort = groupBySort.and(fieldSort);
				} else {
					throw new BadRequestException(Constante.MESSAGE_PARAMETERS_PAGING_INVALID_TEXT);
				}
			}
		}

		return groupBySort;
	}

	/**
	 * Method to validate the search order (ASC(+) o DESC(-))
	 * 
	 * @param orden {@link String}
	 * @return {@link boolean}
	 */
	public static boolean validarOrdenDeBusqueda(String orden) {
		if (orden == null)
			return false;
		if (orden.isEmpty())
			return false;
		if (orden.equals(Constante.ORDEN_ASC_MAS_PARAM) || orden.equals(Constante.ORDEN_DESC_MENOS))
			return true;
		return false;
	}

	/**
	 * Method that allows obtaining the search order (ASC o DESC)
	 * 
	 * @param orden {@link String}
	 * @return {@link String}
	 */
	public static String obtenerOrdenDeBusqueda(String orden) {
		String ordenacion = Constante.EMPTY;
		if (validarOrdenDeBusqueda(orden)) {
			if (orden.equals(Constante.ORDEN_ASC_MAS_PARAM))
				ordenacion = Constante.ORDEN_ASC;
			if (orden.equals(Constante.ORDEN_DESC_MENOS))
				ordenacion = Constante.ORDEN_DESC;
		}

		return ordenacion;
	}

	

	/**
	 * Method that encrypt password
	 * 
	 * @param password
	 * @return {@link String}
	 */
	public static String encryptPassword(String password) {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword(Constante.JASYPT_PASSWORD);
		String myEncryptedText = encryptor.encrypt(password);
		return myEncryptedText;
	}

	/**
	 * Method that decrypt password
	 * 
	 * @param password
	 * @return {@link String}
	 */
	public static String decryptPassword(String password) {
		AES256TextEncryptor encryptor = new AES256TextEncryptor();
		encryptor.setPassword(Constante.JASYPT_PASSWORD);
		String myDecryptText = encryptor.decrypt(password);
		return myDecryptText;
	}

	/**
	 * Method that return name the order status
	 * 
	 * @param orderStatus
	 * @return {@link String}
	 */
	public static String getNameOrderStatus(String orderStatus) {
		String nameOrderStatus = "";

		switch (orderStatus) {
		case Constante.ORDER_STATE_INFORMED:
			nameOrderStatus = "Informado";
			break;
		case Constante.ORDER_STATE_CANCELED:
			nameOrderStatus = "Cancelado";
			break;
		case Constante.ORDER_STATE_ATTENDED:
			nameOrderStatus = "Atendido";
			break;
		case Constante.ORDER_STATE_DELIVERED:
			nameOrderStatus = "Entregado";
			break;
		case Constante.ORDER_STATE_REJECTED:
			nameOrderStatus = "Rechazado";
			break;
		case Constante.ORDER_STATE_SENT:
			nameOrderStatus = "Enviado";
			break;
		default:
			break;
		}
		return nameOrderStatus;
	}

	/**
	 * 
	 * @param latitude1
	 * @param longitude1
	 * @param latitude2
	 * @param longitude2
	 * @return
	 */
	public static double distanceBetweenTwoPoints(double latitude1, double longitude1, double latitude2,
			double longitude2) {
		double factorMilesMeters = 1609.34;
		double minutes = 60;
		double statuteMiles = 1.1515;
		if ((latitude1 == latitude1) && (longitude1 == longitude2)) {
			return 0;
		} else {
			double theta = longitude1 - longitude2;
			double distance = Math.sin(Math.toRadians(latitude1)) * Math.sin(Math.toRadians(latitude2))
					+ Math.cos(Math.toRadians(latitude1)) * Math.cos(Math.toRadians(latitude2))
							* Math.cos(Math.toRadians(theta));
			distance = Math.acos(distance);
			distance = Math.toDegrees(distance);
			distance = distance * minutes * statuteMiles;
			distance = distance * factorMilesMeters;
			return distance;
		}
	}

	public static boolean validateOrderState(String stateOrderRequest, String stateOrderActual) {

		boolean validateOrderState = false;

		if (stateOrderActual.equals(Constante.ORDER_STATE_INFORMED)
				&& stateOrderRequest.equals(Constante.ORDER_STATE_DELIVERED))
			validateOrderState = true;

		return validateOrderState;
	}

	/**
	 * Method that transforms a String object into a Date object in the format it
	 * receives as a parameter
	 * 
	 * @param dFecha
	 * @param sFormato
	 * @return String
	 */
	public static final String fromDateToString(Date dFecha, String sFormato) {
		String sFecha = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(sFormato);
			sFecha = sdf.format(dFecha);
		} catch (Exception e) {
			sFecha = null;
		}
		return sFecha;
	}

	/**
	 * Method that transforms a Date object into a string in the format it receives
	 * as a parameter
	 * 
	 * @param sFecha
	 * @param sFormato
	 * @return Date
	 */
	public static final Date fromStringToDate(String sFecha, String sFormato) {
		Date dFecha = null;
		try {
			if (sFecha != null) {
				SimpleDateFormat sdf = new SimpleDateFormat(sFormato);
				dFecha = sdf.parse(sFecha.trim());
			}
		} catch (Exception e) {
			dFecha = null;
		}
		return dFecha;
	}
	
	public static int getNumberOfDecimalPlaces(BigDecimal bigDecimal) {
	    String string = bigDecimal.stripTrailingZeros().toPlainString();
	    int index = string.indexOf(".");
	    return index < 0 ? 0 : string.length() - index - 1;
	}
}
