/**
 * 
 */
package com.pe.ms.mercado.web.controller.impl;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.RestController;

import com.pe.ms.mercado.core.base.dto.ClaseDto;
import com.pe.ms.mercado.core.base.exception.BadRequestException;
import com.pe.ms.mercado.core.base.exception.EmptyResultException;
import com.pe.ms.mercado.core.base.exception.ErrorFunctionalException;
import com.pe.ms.mercado.core.base.exception.ResourceNotFoundException;
import com.pe.ms.mercado.core.base.mapper.impl.ClaseResourceMapperImpl;
import com.pe.ms.mercado.core.base.resource.ClaseResource;
import com.pe.ms.mercado.core.base.resource.request.RequestPostClase;
import com.pe.ms.mercado.core.base.resource.request.RequestPutClase;
import com.pe.ms.mercado.core.base.resource.response.ResponseDataGetClase;
import com.pe.ms.mercado.core.base.resource.response.ResponseDataPostClase;
import com.pe.ms.mercado.core.base.resource.response.ResponseDataPutClase;
import com.pe.ms.mercado.core.base.resource.response.ResponseGetClase;
import com.pe.ms.mercado.core.base.resource.response.ResponsePostClase;
import com.pe.ms.mercado.core.base.resource.response.ResponsePutClase;
import com.pe.ms.mercado.core.service.ClaseService;
import com.pe.ms.mercado.util.Constante;
import com.pe.ms.mercado.util.Util;
import com.pe.ms.mercado.web.controller.ClaseController;

import io.swagger.annotations.Api;

/**
 * The Class ClaseControllerImpl.
 *
 * @author emaguina
 */
@RestController
@Api(tags = "Classes")
public class ClaseControllerImpl implements ClaseController {

	/** The messages. */
	@Autowired
	private MessageSource messages;

	/** The clase service. */
	@Autowired
	private ClaseService claseService;

	/** The clase mapper. */
	@Autowired
	private ClaseResourceMapperImpl claseMapper;

	/**
	 * Search class in database.
	 *
	 * @param text   {@link String}
	 * @param page   {@link Integer}
	 * @param size   {@link Integer}
	 * @param sortBy {@link String}
	 * @return {@link List<ProductTemplateDto>}
	 * @throws EmptyResultException throw EmptyResultException
	 * @throws BadRequestException  throw BadRequestException
	 */
	@Override
	public ResponseGetClase getClasses(String text, @NotNull String page, @NotNull String size, @NotNull String sortBy)
			throws EmptyResultException, BadRequestException {
		ResponseGetClase response = new ResponseGetClase();


		if (page == null && size == null) {
			page = Constante.NUMERO_PAGINA;
			size = Constante.TAMANO_PAGINA;
		}

		int numeroPag = Util.convertirStringInt(page);
		int tamanioPag = Util.convertirStringInt(size);

		if (sortBy == null || sortBy.equals("")) {
			sortBy = "classId";
		} else {
			String[] arrayJson = sortBy.split(Constante.COMA);
			String dinamicSortBy = Util.getFielfromJsonProperty(arrayJson, Constante.CLASS_BASE_RESOURCE, "base");
			sortBy = dinamicSortBy.isEmpty() ? sortBy : dinamicSortBy;
		}

		List<ClaseDto> classDtoList = claseService.getClasses(text, numeroPag, tamanioPag, sortBy);

		String message = messages.getMessage(Constante.MESSAGE_CLASSES_GET_LIST, null, LocaleContextHolder.getLocale());

		ResponseDataGetClase data = new ResponseDataGetClase();
		data.setClasses(claseMapper.asClaseResources(classDtoList));
		data.setTotal(claseService.getCountClasses());

		response.setMessage(message);
		response.setData(data);

		return response;
	}

	/**
	 * Create a class.
	 *
	 * @param requestPostClaseDto {@link RequestPostClase}
	 * @return {@link ResponsePostClase}
	 * @throws ResourceNotFoundException throw ResourceNotFoundException
	 * @throws BadRequestException       throw BadRequestException
	 * @throws ErrorFunctionalException  throw ErrorFunctionalException
	 */
	@Override
	public ResponsePostClase createClass(@Valid RequestPostClase requestPostClaseDto)
			throws ResourceNotFoundException, BadRequestException, ErrorFunctionalException {

		if (requestPostClaseDto == null)
			throw new BadRequestException(
					messages.getMessage(Constante.MESSAGE_BODY_IS_REQUIRED, null, LocaleContextHolder.getLocale()));

		ClaseDto classDto = claseService.createClass(claseMapper.asClaseDto(requestPostClaseDto));
		ResponsePostClase response = new ResponsePostClase();
		ResponseDataPostClase data = new ResponseDataPostClase();
		ClaseResource claseResource = claseMapper.asClaseResource(classDto);
		data.setClase(claseResource);
		response.setMessage(
				messages.getMessage(Constante.MESSAGE_CLASS_CREATED_SUCCESS, null, LocaleContextHolder.getLocale()));
		response.setData(data);
		return response;
	}

	/**
	 * Update clase.
	 *
	 * @param customerId    the customer id
	 * @param requestPutDto the request put dto
	 * @return the response put clase
	 * @throws ResourceNotFoundException the resource not found exception
	 * @throws BadRequestException       the bad request exception
	 * @throws ErrorFunctionalException  the error functional exception
	 */
	@Override
	public ResponsePutClase updateClase(Long customerId, @Valid RequestPutClase requestPutDto)
			throws ResourceNotFoundException, BadRequestException, ErrorFunctionalException {
		if (requestPutDto == null)
			throw new BadRequestException(
					messages.getMessage(Constante.MESSAGE_BODY_IS_REQUIRED, null, LocaleContextHolder.getLocale()));

		ClaseDto classDto = claseService.updateClass(customerId, claseMapper.asClaseDto(requestPutDto));
		ResponsePutClase response = new ResponsePutClase();
		ResponseDataPutClase data = new ResponseDataPutClase();
		data.setClase(claseMapper.asClaseResource(classDto));
		response.setMessage(
				messages.getMessage(Constante.MESSAGE_CLASS_UPDATED_SUCCESS, null, LocaleContextHolder.getLocale()));
		response.setData(data);
		return response;
	}
}
