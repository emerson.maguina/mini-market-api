﻿--COUNTRY
INSERT INTO public.country(country_id, currency_id, name, telf_id)
	VALUES ('PE', 'PEN', 'PERU', '51');

--ESTABLISHMENT TYPE
INSERT INTO public.establishment_type(
	establishment_type_id, name)
	VALUES (1, 'Bodega');
INSERT INTO public.establishment_type(
	establishment_type_id, name)
	VALUES (2, 'Puesto de Mercado');

--SCHEDULE TEMPLATE
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (1, 'Lun - Vie de 8 am - 7 pm, Sab de 9 am - 1 pm', 'N');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (2, 'Lun - Sab de 9 am - 6 pm', 'N');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (3, 'Todos los días de 9 am - 4 pm', 'N');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (4, 'De 8:00 a 9:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (5, 'De 9:00 a 10:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (6, 'De 11:00 a 12:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (7, 'De 12:00 a 13:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (8, 'De 13:00 a 14:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (9, 'De 14:00 a 15:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (10, 'De 15:00 a 16:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (11, 'De 16:00 a 17:00', 'S');
INSERT INTO public.schedule_template(
	shipping_schedule_id, range, type)
	VALUES (12, 'De 17:00 a 18:00', 'S');
	
--CLASS
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (1,'Abarrotes','Abarrotes',NULL);
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (2,'Frutas','Frutas',NULL);
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (3,'Verduras','Verduras',NULL);
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (4,'Quesos, Fiambres','Quesos, Fiambres',NULL);
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (5,'Desayunos','Desayunos',NULL);	
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (6,'Panes y Pasteleria','Panes y Pasteleria',NULL);	
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (7,'Helados y Congelados','Helados y Congelados',NULL);	
INSERT INTO public.class(
	class_id, description, name, path_image)
	VALUES (8,'Carne','Carne',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (9,'Pollo','Pollo',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (10,'Pescados','Pescados',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (11,'Aseo Personal','Aseo Personal',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (12,'Limpieza','Limpieza',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (13,'Bebidas','Bebidas',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (14,'Cervezas y Licores','Cervezas y Licores',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (15,'Mascotas','Mascotas',NULL);	
INSERT INTO class(
	class_id, description, name, path_image)
	VALUES (16,'Articulos Utiles','Articulos Utiles',NULL);	
	
--CATEGORY
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (1,'Arroz','Arroz',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (2,'Aceites','Aceites',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (3,'Fideos','Fideos',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (4,'Salsas','Salsas',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (5,'Conservas','Conservas',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (6,'Menestras y Legumbres','Menestras y Legumbres',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (7,'Harinas y Feculas','Harinas y Feculas',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (8,'Pures, Sopas y Sustancias','Pures, Sopas y Sustancias',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (9,'Condimentos','Condimentos',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (10,'Vinagres, Salsas, Aliños','Vinagres, Salsas, Aliños',NULL,1);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (11,'Azucar y Sustitutos','Azucar y Sustitutos',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (12,'Café e Infusiones','Café e Infusiones',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (13,'Cereales','Cereales',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (14,'Mermeladas, Mieles y afines','Mermeladas, Mieles y afines',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (15,'Panes','Panes',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (16,'Cereales','Cereales',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (17,'Modificadores Lacteos','Modificadores Lacteos',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (18,'Galletas, Golosinas, Snacks','Galletas, Golosinas, Snacks',NULL,5);
INSERT INTO public.category(
	category_id, description, name, path_image, class_id)
	VALUES (19,'Reposteria','Reposteria',NULL,5);	
	
--SUBCATEGORY
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (1,'Arroz Extra','Arroz Extra',NULL,1);
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (2,'Arroz Superior','Arroz Superior',NULL,1);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (3,'Arroz Integral','Arroz Integral',NULL,1);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (4,'Arroz Especial','Arroz Especial',NULL,1);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (5,'Aceite Vegetal / Soya','Aceite Vegetal / Soya',NULL,2);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (6,'Aceite de Maiz','Aceite de Maiz',NULL,2);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (7,'Aceite de Girasol','Aceite de Girasol',NULL,2);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (8,'Aceite de Oliva','Aceite de Oliva',NULL,2);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (9,'Otros Aceites','Otros Aceites',NULL,2);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (10,'Fideos Largos','Fideos Largos',NULL,3);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (11,'Fideos Cortos','Fideos Cortos',NULL,3);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (12,'Semolas y especiales','Semolas y especiales',NULL,3);	
INSERT INTO public.sub_category(
	sub_category_id, description, name, path_image, category_id)
	VALUES (13,'Pastas Integrales','Pastas Integrales',NULL,3);		
	
--PRODUCTS TEMPLATE
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (1,'FA','Faraon','Faraon',NULL,'A','Kg',1,1,1,NULL);	
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (2,'VA','Valle Norte','Valle Norte',NULL,'A','Kg',1,1,1,NULL);	
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (3,'CO','Costeño','Costeño',NULL,'A','Kg',1,1,1,NULL);	
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (4,'PA','Paisana','Paisana',NULL,'A','Kg',1,1,1,NULL);	
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (5,'PA','Paisana','Paisana',NULL,'A','KG',1,1,3,NULL);	
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (6,'SCO','Arroz Arborio Scotti','Arborio Scotti',NULL,'A','Kg',1,1,4,NULL);	
INSERT INTO public.product_template(
	product_template_id, code, description, name, path_image, status, unit_measure, category_id, class_id, sub_category_id, sub_sub_category_id)
	VALUES (7,'ASI','Arroz Asiatico Macador','Asiatico Macador',NULL,'A','Kg',1,1,4,NULL);	
		
